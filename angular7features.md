what are the updates in angular7 ?
1. CLI prompts:
In angular 7, the command line interface (CLI) prompts has been modernized to version 7.0.2.
With the new and updated version of angular CLI comes new features. 
When the user executes common commands like ng add @angular/material or ng new, 
it will prompt users. It helps users to explore the in-built SCSS support, routing, etc.
To benefit all packages that publish schematics,
CLI prompts have been added to schematics too.

2. Application Performance:
Angular 7 is faster as compared to the previous versions of angular. 
Certain new features introduced as discussed above makes the app run faster and perform better. 
It does not only focus on making framework small but makes the app small too as much as possible. 
In version 7 the reflect-metadata polyfill (includes in production) remove automatically.

3. Virtual Scrolling:
Scrolling feature was present in the previous version of angular (angular 6). 
Angular 7 comes with virtual scrolling. 
Virtual scrolling allows users to bind a list of elements even its too long in small packets. 
As per the scroll movements, data will be supplied into packets at the user end. 
This is one of the Angular 7 features, 
which is beneficial for mobile apps where scrolling can affect performance. 
But useful for web applications too as even in web applications if we scroll a long list of data, 
performance gets affected.

4. Drag and Drop:
Now even by dragging and dropping items in the list user can re-order the list. 
It also allows the transfer of elements between the lists. 
Using CDK drag handle, a user can customize the drag area as per requirement. 
Multiple handles apply on a single item if requires. As per the user’s command, 
the item may move along the X-axis or the Y-axis because the movement of drag and drop restricts along an axis.

5. Bundle Budget:
While developing applications on Angular 7, now the developers can set up a budget limit of their bundle size.
a default setting of the bundled budget has 2 MB as the lower limit and 5 MB as the higher limit.
When the initial bundle is more than 2MB, a new application will warn and will error at 5 MB. 
The developer can also change these settings as per need. 
Reduction in bundle size improves the performance of the application.

6. Angular Compiler
The new angular compiler offers an 8-phase rotating ahead-of-time compilation. 
The NGCC (Angular Compatibility Compiler) will convert node_modules compiled with Angular Compatibility 
Compiler (NGCC), into node_modules which appear to have been composed with TSC compiler transformer (NTSC). 
Also, Ivy rendering engine can use such “legacy” package due to this compiler change

7. Angular Elements
It was introduced in the Angular 6. Since a part of the Angular framework, 
the angular 7 comes with its new updates. 
Angular now supports content projection using web standard for custom elements.

8. Angular Do-Bootstrap
It uses to bootstrap those modules which require bootstrap of components. 
Angular 7 offers interface (DoBootstrap) and new life-cycle hoop (ngDoBootstrap).

9. Better Error Handling
Angular 7 improves error handling in an angular application.
 @Output in angular7.0 has an improved error handling feature.

10. Native Script
Before Angular 7 developers have to create separate projects for mobile and web version of the application
but Now through a single project, users can build a web and mobile app too. 
A native script schematics collection provide this functionality. 
The codes for the web and mobile apps will maintain in such a way that the shareable part keep at one
place and non-shareable ones can create separately but in a single project.