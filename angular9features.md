Ivy
Version 9 moves all applications to use the Ivy compiler and runtime by default. In addition to hundreds of bug fixes, the Ivy compiler and runtime offers numerous advantages:
-->Smaller bundle sizes
-->Faster testing
-->Better debugging
-->Improved CSS class and style binding
-->Improved type checking
-->Improved build errors
-->Improved build times, enabling AOT on by default
-->Improved Internationalization
Here’s a breakdown of some of the more notable improvements.
1.Smaller bundle sizes:
The Ivy compiler has been designed to remove parts of Angular that aren’t being used via tree-shaking and to generate less code for each Angular component.
With these improvements, small apps and large apps can see the most dramatic size savings.
Small apps that don’t use many Angular features can benefit most from tree-shaking.
Large apps with many components can benefit most from the reduced factory size.
Medium-sized apps should see bundle sizes that are on par or slightly smaller, since they benefit less from tree-shaking and don’t have enough components to truly leverage smaller factories.

2.Faster testing:
We have also revamped the implementation of TestBed in Ivy to make it more efficient.
Previously, TestBed would recompile all components between the running of each test, regardless of whether there were any changes made to components (for example, through overrides).
In Ivy, TestBed doesn’t recompile components between tests unless a component has been manually overridden, which allows it to avoid recompilation between the grand majority of tests.
With this change, the framework’s core acceptance tests are about 40% faster. We would expect users to see their own application test speeds to be around 40–50% faster.
Better debugging
Ivy provides you with more tools to debug your applications. When running an application in Dev Mode with the Ivy runtime, we now offer the new ng object for debugging.
You can ask Angular for access to instances of your components, directives, and more
You can manually call methods and update state
When you want to see the results of change detection, you can trigger change detection with applyChanges

3.Improved CSS class and style binding:
The Ivy compiler and runtime provides improvements for handling styles. 
Previously, if an application contained competing definitions for a style, 
those styles would destructively replace each other. 
With Ivy, the styles are merged in a predictable way.
With version 9, you can manage your styles through a clear, 
consistent order of precedence that isn’t dependent on timing. 
The most specific styles always have the highest precedence. 
For example, a binding to [style.color] overrides a conflicting binding to [style].

4.Improved type checking:
The Angular compiler can check more of the types of your application, and it can apply more strict rules. These features will help you and your team catch bugs earlier in the development process.
We support two main flags for additional type checks in addition to the default:
fullTemplateTypeCheck — Activating this flag tells the compiler to check everything within your template (ngIf, ngFor, ng-template, etc)
strictTemplates — Activating this flag will apply the strictest Type System rules for type checking.

5.Improved build errors:
The new Ivy compiler is not only faster and offers stronger type safety, it also makes all of the error messages easier to read.

6.Improved build times, enabling Ahead-of-Time compiler by default:
 we’ve made significant improvements to the compiler’s performance.
We measure our compiler’s performance in terms of the overhead on top of a plain TypeScript compilation of an application. For our documentation app (angular.io), this overhead decreased from 0.8x to 0.5x with Ivy, an improvement of nearly 40%.
These improvements mean that AOT builds can be noticeably faster. Thanks to this speedup, for the first time ever we’re using AOT even for dev-mode builds. This means that `ng serve` now benefits from the same compile-time checking as production builds, significantly improving the developer experience for Angular.

7.Improved internationalization (i18n):
Internationalization has been a core feature of Angular, where you could build your application once per locale and receive highly optimized and localized applications. In 9.0, we’re making this faster by moving the build-time i18n substitutions later in the build process. This change allowed us to make it up to 10 times faster.

8.More reliable ng update:
We’ve made some changes to how ng update works to make it more reliable and informative.
Always use the latest CLI. Starting with 8.3.19 of the CLI, we now use the CLI from the destination version during updates. This means that going forward, updates will take advantage of newer update features automatically.
Clearer progress updates. ng update now does more to tell you what is going on under the hood. For each migration, you’ll see information about the migration.
Easier update debugging. By default, ng update runs all of the migrations and leaves the aggregate changes on disk for you to inspect. The version 9 update also introduces the new --create-commits flag. When you run ng update --create-commits, the tool commits the state of your codebase after each migration, so you can step through and understand or debug the changes we are making to your code.

9.New options for 'providedIn':
When you create an @Injectable service in Angular, you must choose where it should be added to the injector. In addition to the previous root and module options, you have two additional options.
platform— Specifying providedIn: 'platform' makes the service available in a special singleton platform injector that is shared by all applications on the page.
any— Provides a unique instance in every module (including lazy modules) that injects the token.

10.Component harnesses
Testing components has historically relied on using implementation details such as CSS selectors to find components and to trigger events. This meant that whenever a component library changed its implementation, all of the tests relying on those components would need to be updated.
In version 9, we are introducing component harnesses, which offer an alternative way to test components. By abstracting away the implementation details, you can make sure your unit tests are correctly scoped and less brittle.
Most of Angular Material’s components can now be tested via harnesses, and we are making harnesses available to any component author as part of the Component Dev Kit (CDK).

11.New components:
You can now include capabilities from YouTube and Google Maps in your applications.
You can render a YouTube Player inline within your application with the new youtube-player. After you load the YouTube IFrame player API, this component will take advantage of it.
We are also introducing google-maps components. These components make it easy to render Google Maps, display markers, and wire up interactivity in a way that works like a normal Angular component, saving you from needing to learn the full Google Maps API.

12.IDE & language service improvements:
Significant improvements have been made to the Angular language service extension on the Visual Studio Marketplace. Along with major architectural overhaul to address performance and stability issues, many long-standing bugs have also been fixed. Besides that, some new features include:
TextMate grammar for Angular Template Syntax, which now enables syntax highlighting in both inline and external templates
“Go to definition” for templateUrl and styleUrls
NgModule and type information in hover tooltip.

13.TypeScript 3.7 support
Angular has been updated to work with TypeScript 3.6 and 3.7, including the extremely popular optional chaining feature in TypeScript 3.7. To stay in sync with the ecosystem, we’ve also updated our version of other ecosystem dependencies such as Zone.JS and RxJS.

