1.what is rxjs ?
 RxJS (Reactive Extensions for JavaScript) is a library for reactive programming
 using observables that makes it easier to compose asynchronous or callback-based code.

2.what is observable in rxjs ?
 Observable: represents the idea of an invokable collection of future values or events.
 Observables are lazy Push collections of multiple values.
syntax:
   import { Observable } from 'rxjs';
    const observable = new Observable( subscriber => {
        subscriber.next('this is asim');
        subscriber.error('testing');
        subscriber.complete()
      }
    });
    observable.subscribe(x => console.log(x));
note:
To invoke the Observable and see these values, we need to subscribe to it.
observable is also like a function.
we need unsubcribe() finally.

3.what is observer in rxjs ?
 Observer: is a collection of callbacks that knows how to listen to values delivered by the Observable.
 An Observer is a consumer of values delivered by an Observable.
syntax:
	const observer = {
  next: x => console.log('Observer got a next value: ' + x),
  error: err => console.error('Observer got an error: ' + err),
  complete: () => console.log('Observer got a complete notification'),
};
observable.subscribe(observer);
note:
To use the Observer, provide it to the subscribe of an Observable:

