what are the new features in angular 10 ?
1.Angular Material now includes a new date range picker.
To use the new date range picker:
you can use the mat-date-range-input and mat-date-range-picker components.
To view the example: 
--->1.https://stackblitz.com/angular/nknyovevygv?file=src%2Fapp%2Fdate-range-picker-overview-example.html
--->2.https://next.material.angular.io/components/datepicker/overview#date-range-selection.

2.Warnings about CommonJS imports:
When you use a dependency that is packaged with CommonJS, 
it can result in larger slower applications.

3.Optional Stricter Settings:
Version 10 offers a more strict project setup when you create a new workspace with ng new.
cmd--->ng new --strict.
Enabling this flag initializes your new project with a few new settings that improve maintainability, help you catch bugs ahead of time, and allow the CLI to perform advanced optimizations on your app. Specifically, the strict flag does the following:
Enables strict mode in TypeScript
Turns template type checking to Strict
Default bundle budgets have been reduced by ~75%
Configures linting rules to prevent declarations of type any
Configures your app as side-effect free to enable more advanced tree-shaking.

4.Keeping Up to Date with the Ecosystem:
As usual, we have made a few updates to the dependencies of Angular to stay synchronized with the JavaScript ecosystem.
-->TypeScript bumped to TypeScript 3.9
-->TSLib has been updated to v2.0
-->TSLint has been updated to v6
We’ve also updated our project layout. Starting with version 10 you will see a new tsconfig.base.json. This additional tsconfig.json file better supports the way that IDEs and build tooling resolve type and package configurations.

5.New Default Browser Configuration:
We’ve updated the browser configuration for new projects to exclude older and less used browsers.

6.Angular Team Fixit
We’ve dramatically increased our investment in working with the community. 
In the last three weeks our open issue count has decreased by over 700 issues across framework, tooling, and components. 
We’ve touched over 2,000 issues, and we plan to make large investments over the next few months, working with the community to do even more.

7.Deprecations and Removals
We’ve made several new deprecations and removals from Angular.
The Angular Package Format no longer includes ESM5 or FESM5 bundles, 
saving you 119MB of download and install time when running yarn or npm install for Angular packages and libraries. These formats are no longer needed as any downleveling to support ES5 is done at the end of the build process.
Based on heavy consultation with the community, 
we are deprecating support for older browsers including IE 9, 10, and Internet Explorer Mobile.

8.You can read more about our deprecations and removals:
https://angular.io/guide/deprecations