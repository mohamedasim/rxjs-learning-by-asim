what are the features in angular 8 ?

1 – Angular Ivy:
For over a year, the Angular Team has been telling about their new Ivy renderer, 
so it is obviously the major part of the Angular 8 update.
Ivy renderer is basically the new compiler of Angular framework. 
Though our Angular developers were really hoping for the final release of the new compiler, 
but unfortunately the Ivy renderer is only available as an opt-in preview in Angular 8 update.
Compared with the current Angular compiler, Ivy renderer will offer the following benefits:
Template type checking will be improved, which will allow catching more errors at build time
Payload time will be decreased, which will help browser download and parse applications faster
Rebuild times will also get faster
The best part is, Angular team has planned to make Ivy renderer compatible with existing Angular applications, 
meaning you won’t have to make major changes once it is finally released.

2 – Router: 
Location
A bunch of new stuff has been added to the location services in the new Angular 8 update.
Platformlocation, for example, provides access to the protocol, port, and hostname.
There is also a new getstate() method, which will help developers get the history.state.
All of these updates are really useful if you’re using ngUpgrade.
Lazy Loading with Import() Sytax
The Angular 8 update includes a new way to declare lazy-loading routes using the TypeScript syntax – import().
In the latest Angular version, this will be the preferred way of declaring a lazy-loading route
and the Ivy renderer will only support this.
Here’s how you can change the lazy-loading route for Angular 8 for example:
Old way:
loadChildren: ‘.admin/admin.module#AdminTestModule’
New Way:
loadChildren: () => import(‘./races/races.module’).then(m => m.RacesModule)
As you can see, the schematic offered by CLI automatically migrates your declarations, 
making it easier to run ng update @angular.cli.

3 – Forms:
FormArray.clear
The FormArray in the new Angular 8 now offers a clear method so 
that it becomes quicker to remove all the control it contains.
In earlier version of Angular, developers had loop over these controls and remove them one by one.
// `customers` is initialized with 2 customers
const customers = fb.array([customer1, customer2]);
customers.clear();
// customers is now empty
markAllAs Touched
The AbstractControl class in Angular now has a new method – markAllAsTouched, 
in addition to the existing markAsTouched, markAsPending, and markAsDirty methods.
As the name suggests, the markAllAs Touched method will mark an AbstractControl as 
touched and also its descendants.
AbstractControl is basically the parent class of FormControl, FormArray, 
and FormGroup, making the method available on all of the reactive form entities.
Here’s the syntax to use this new method:
form.markAllAsTouched();

3 – Support for TypeScript:
Angular is finally going to include updates to the latest version of Angular’s dependencies, 
which are mainly TypeScript and RxJS.
In fact, it is now mandatory to upgrade to TypeScript 3.4 in the Angular 8 update.
Though it may look like just a small improvement, 
but if you look at the positive side then this update is extremely good since the TypeScript
team always introduces new features in their every new release.

4 – Service Worker:
Registration Strategy
The service worker registration in the Angular 8 update now has a new option to specify when 
the registration should take place. Earlier, the service worker had to wait for the app to be stable 
in order to avoid slowing the start of the application.
For example, if you had run a recurring asynchronous task in the previous version of Angular, 
the service worker would never get registered as Angular would consider application to be not stable 
due to the recurring task.
With Angular 8, however, you can use the new registrationStrategy option to 
handle the registration of the service worker.
For instance, suppose you want to register the service worker after 5 seconds. 
Here’s how you would do it in Angular 8.
providers: [
ServiceWorkerModule.register(‘/ngsw-worker.js’, {
enabled: environment.production,
registrationStrategy: ‘registerDelay:5000’
}),
// …
]
Bypass a Service Worker
In the new Angular 8 update, you can now also bypass the service worker for a particular 
request using the new ngsw-bypass header.
this.http.get(‘api/users’, { headers: { ‘ngsw-bypass’: true } });
Multiple Apps on Sub-Domains
In the earlier version of Angular, developers had no option to have multiple 
apps use @angular/service-worker on different sub-domains because that would cause the 
service worker to overwrite the caches of other sub-domains.
Now in Angular 8, this glitch has been fixed.

5 – Support for Bazel:
Bazel in Angular 8 is an opt-in option for now, but it is expected to be included in the 
Angular CLI in version 9.
Bazel basically offers fast build time, which means you can build your application through an 
incremental build and deploy the last changes only.

6 – No More @angular/http:
The Angular 8 will no longer support @angular/http. 
So, if you have an existing Angular application built using the previous version, 
then you need to make appropriate adjustments to the code and replace @angular/http with @angular/common/http.

7 – Builders API:
The Angular 8 update has now added support to use Builders API, also referred as Architect API.
Builder is basically just a function with a bunch of commands that you pass to createbuilder() 
method through the @angular-devkit/architect package.
These builders are basically used for the following main operations:

Serve
Build
Test
Lint
e2e
In addition, developers can now also create their own custom builders in addition 
to the built-in builders from here.
